package unibuc.programweb.proiect.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import unibuc.programweb.proiect.dtos.CustomerDTO;
import unibuc.programweb.proiect.dtos.OrderDTO;
import unibuc.programweb.proiect.dtos.OrderForCustomerDTO;
import unibuc.programweb.proiect.errors.CustomException;
import unibuc.programweb.proiect.mapper.CustomerMapper;
import unibuc.programweb.proiect.mapper.OrderMapper;
import unibuc.programweb.proiect.model.Customer;
import unibuc.programweb.proiect.model.Order;
import unibuc.programweb.proiect.repository.CustomerRepository;
import unibuc.programweb.proiect.repository.OrderRepository;
import unibuc.programweb.proiect.repository.ProductRepository;
import unibuc.programweb.proiect.repository.SupplierRepository;
import unibuc.programweb.proiect.service.impl.OrderServiceImpl;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {

    @Mock
    OrderRepository orderRepository;

    @Mock
    OrderMapper orderMapper;

    @Mock
    ProductRepository productRepository;

    @Mock
    CustomerRepository customerRepository;

    @Mock
    CustomerMapper customerMapper;

    @Mock
    SupplierRepository supplierRepository;

    @InjectMocks
    OrderServiceImpl orderService;

    @Test
    @DisplayName("find orders")
    void getOrders() {
        List<Order> initialOrder = returnOrders();
        when(orderRepository.findAll()).thenReturn(returnOrders());
        when(orderMapper.toDto(any(Order.class))).thenReturn(returnOrdersDTO().get(0));
        List<OrderDTO> orders = orderService.getOrders();

        assertEquals(orders.get(0).getOrderDetails(), initialOrder.get(0).getOrderDetails());
    }

    @Test
    @DisplayName("find order by customer - throw exception")
    void findOrderByCustomerTestWithException() {
        CustomException exception = assertThrows(CustomException.class, () ->
                orderService.findOrderByCustomer(null));

        assertEquals(HttpStatus.BAD_REQUEST, exception.getHttpStatus());
        assertEquals("Customer not found", exception.getErrorMessage());
    }

    @Test
    @DisplayName("find order by customer")
    void findOrderByCustomerTest() {
        List<Order> initialOrders = returnOrders();

        when(orderRepository.findAllByCustomerId(any())).thenReturn(returnOrders());
        when(orderMapper.toDto(any(Order.class))).thenReturn(returnOrdersDTO().get(0));
        List<OrderDTO> orderByCustomer = orderService.findOrderByCustomer(UUID.randomUUID());

        assertEquals(initialOrders.size(), initialOrders.size());
        assertEquals(initialOrders.get(0).getOrderDetails(), orderByCustomer.get(0).getOrderDetails());
    }

    @Test
    @DisplayName("create order for null customer - throw exception")
    void createOrderForNullCustomer() {
        CustomException customException = assertThrows(CustomException.class, () -> {
            orderService.createOrderForSpecificCustomer(returnOrderForCustomerDTO(null));
        });

        assertEquals(HttpStatus.BAD_REQUEST, customException.getHttpStatus());
        assertEquals("Customer could not be empty", customException.getErrorMessage());
    }

    @Test
    @DisplayName("create order for non-existing customer - throw exception")
    void createOrderForNonExistingCustomer() {
        UUID uuid = UUID.randomUUID();
        when(customerRepository.findById(uuid)).thenReturn(Optional.empty());
        CustomException customException = assertThrows(CustomException.class, () -> {
            orderService.createOrderForSpecificCustomer(returnOrderForCustomerDTO(uuid));
        });

        assertEquals(HttpStatus.NO_CONTENT, customException.getHttpStatus());
        assertEquals("Customer not found", customException.getErrorMessage());
    }

    @Test
    @DisplayName("create order for customer")
    void createOrderSpecificCustomer() {
        UUID uuid = UUID.randomUUID();
        Customer initialCustomer = returnCustomer();
        when(customerRepository.findById(uuid)).thenReturn(Optional.of(returnCustomerWithOrder()));
        when(orderRepository.saveAll(anyList())).thenReturn(returnOrders());
        when(orderMapper.toEntity(any(OrderDTO.class))).thenReturn(returnOrders().get(0));
        when(customerMapper.toDto(any(Customer.class))).thenReturn(returnCustomerDTOWithOrderFinal());
        when(customerRepository.save(any(Customer.class))).thenReturn(returnCustomerWithOrderFinal());
        CustomerDTO orderForSpecificCustomer = orderService.createOrderForSpecificCustomer(returnOrderForCustomerDTO(uuid));

        assertNotEquals(initialCustomer.getOrders().size(), orderForSpecificCustomer.getOrders().size());
    }

    private List<Order> returnOrders() {
        Order order = new Order();
        order.setOrderDetails("TEST_ORDER_DETAILS");
        return Collections.singletonList(order);
    }

    private List<OrderDTO> returnOrdersDTO() {
        OrderDTO order = new OrderDTO();
        order.setOrderDetails("TEST_ORDER_DETAILS");
        return Collections.singletonList(order);
    }

    private OrderForCustomerDTO returnOrderForCustomerDTO(UUID customerId) {
        OrderForCustomerDTO orderForCustomerDTO = new OrderForCustomerDTO();
        orderForCustomerDTO.setCustomerId(customerId);
        orderForCustomerDTO.setOrder(returnOrdersDTO().get(0));
        return orderForCustomerDTO;
    }

    private Customer returnCustomer() {
        Customer customer = new Customer();
        customer.setName("TEST_NAME");
        customer.setEmail("TEST_EMAIL");
        return customer;
    }

    private Customer returnCustomerWithOrder() {
        Customer customer = new Customer();
        customer.setName("TEST_NAME");
        customer.setEmail("TEST_EMAIL");
        customer.setOrders(createOrders());
        return customer;
    }

    private Customer returnCustomerWithOrderFinal() {
        Customer customer = new Customer();
        customer.setName("TEST_NAME");
        customer.setEmail("TEST_EMAIL");
        Set<Order> order = createOrders();
        order.add(returnOrders().get(0));
        customer.setOrders(order);
        return customer;
    }

    private CustomerDTO returnCustomerDTOWithOrderFinal() {
        CustomerDTO customer = new CustomerDTO();
        customer.setName("TEST_NAME");
        customer.setEmail("TEST_EMAIL");
        List<OrderDTO> orderDTOS = createOrderDTOS();
        orderDTOS.add(returnOrdersDTO().get(0));
        customer.setOrders(orderDTOS);
        return customer;
    }

    private List<OrderDTO> createOrderDTOS() {
        List<OrderDTO> orders = new ArrayList<>();
        OrderDTO order = new OrderDTO();
        order.setOrderDetails("INITIAL_ORDER");
        orders.add(order);
        return orders;
    }

    private Set<Order> createOrders() {
        Set<Order> orders = new HashSet<>();
        Order order = new Order();
        order.setOrderDetails("INITIAL_ORDER");
        orders.add(order);
        return orders;
    }

    private CustomerDTO returnCustomerDTO() {
        CustomerDTO customer = new CustomerDTO();
        customer.setName("TEST_NAME");
        customer.setEmail("TEST_EMAIL");
        return customer;
    }
}