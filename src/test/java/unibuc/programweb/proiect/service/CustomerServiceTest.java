package unibuc.programweb.proiect.service;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import unibuc.programweb.proiect.dtos.AddressDTO;
import unibuc.programweb.proiect.dtos.CustomerDTO;
import unibuc.programweb.proiect.errors.CustomException;
import unibuc.programweb.proiect.mapper.CustomerMapper;
import unibuc.programweb.proiect.model.Address;
import unibuc.programweb.proiect.model.Customer;
import unibuc.programweb.proiect.repository.AddressRepository;
import unibuc.programweb.proiect.repository.CustomerRepository;
import unibuc.programweb.proiect.service.impl.CustomerServiceImpl;
import unibuc.programweb.proiect.utils.CustomerServiceTestUtils;

import java.util.*;
import java.util.regex.Matcher;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {


    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private CustomerMapper customerMapper;

    @Mock
    private AddressRepository addressRepository;

    @Mock
    private OrderService orderService;

    @InjectMocks
    private CustomerServiceImpl customerService;

    @Test
    public void getCustomersTest() {
        when(customerRepository.findAll()).thenReturn(CustomerServiceTestUtils.returnCustomers());

        List<CustomerDTO> allCustomers = customerService.findAllCustomers();

        assertNotEquals(allCustomers.size(), 0);
    }

    @Test
    public void createCustomerTest() {
        CustomerDTO customerDTO = returnDTOCustomer();

        when(customerRepository.save(any(Customer.class))).thenReturn(returnCustomer());
        when(customerMapper.toEntity(any(CustomerDTO.class))).thenReturn(returnCustomer());
        when(customerMapper.toDto(any(Customer.class))).thenReturn(returnDTOCustomer());
        CustomerDTO customer = customerService.createCustomer(customerDTO);

        assertEquals(customerDTO.getEmail(), customer.getEmail());
        assertEquals(customerDTO.getName(), customer.getName());
        assertEquals(customerDTO.getPhoneNumber(), customer.getPhoneNumber());
    }

    @Test
    public void findCustomerByCityTest() {
        List<Customer> customerWithAddress = returnCustomerWithAddress();

        when(customerRepository.findAllByAddressesCity("Bucuresti")).thenReturn(returnCustomerWithAddress());
        when(customerMapper.toDto(anyList())).thenReturn(returnCustomerDTOWithAddress());

        List<CustomerDTO> customers = customerService.findCustomersByCity("Bucuresti");

        assertEquals(customers.get(0).getAddressDTOS().get(0).getCity(), new ArrayList<>(customerWithAddress.get(0).getAddresses()).get(0).getCity());

    }

    @Test
    public void updateCustomerTest() {

        CustomException exception1 = assertThrows(CustomException.class, () ->
                customerService.updateCustomer(null));
        assertEquals("No customer present", exception1.getErrorMessage());
        CustomerDTO customerDTO = returnDTOCustomer();

        when(customerRepository.findById(any())).thenReturn(Optional.empty());
        CustomException exception2 = assertThrows(CustomException.class, () ->
                customerService.updateCustomer(customerDTO));
        assertEquals(HttpStatus.NOT_FOUND, exception2.getHttpStatus());

        when(customerRepository.findById(any())).thenReturn(Optional.of(CustomerServiceTestUtils.returnCustomers().get(0)));
        when(customerMapper.toDto(any(Customer.class))).thenReturn(customerDTO);
        when(customerRepository.save(any())).thenReturn(CustomerServiceTestUtils.returnCustomers().get(0));
        CustomerDTO updateCustomer = customerService.updateCustomer(customerDTO);

        assertNotEquals(updateCustomer, null);

    }

    private CustomerDTO returnDTOCustomer() {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setEmail("TEST");
        customerDTO.setId(UUID.randomUUID());
        customerDTO.setName("TEST");
        customerDTO.setPhoneNumber(15313498);
        return customerDTO;
    }

    private Customer returnCustomer() {
        Customer customer = new Customer();
        customer.setEmail("TEST");
        customer.setId(UUID.randomUUID());
        customer.setName("TEST");
        customer.setPhoneNumber(15313498);
        return customer;
    }

    private List<Customer> returnCustomerWithAddress() {
        Set<Address> addresses = new HashSet<>();
        Address address = new Address();
        address.setCity("Bucuresti");
        address.setAddress("TEST");
        address.setPostalCode("123");
        addresses.add(address);

        Customer customer = new Customer();
        customer.setEmail("TEST");
        customer.setAddresses(addresses);
        customer.setId(UUID.randomUUID());
        customer.setName("TEST");
        customer.setPhoneNumber(15313498);
        return Collections.singletonList(customer);
    }

    private List<CustomerDTO> returnCustomerDTOWithAddress() {
        List<AddressDTO> addresses = new ArrayList<>();
        AddressDTO address = new AddressDTO();
        address.setCity("Bucuresti");
        address.setAddress("TEST");
        address.setPostalCode("123");
        addresses.add(address);

        CustomerDTO customer = new CustomerDTO();
        customer.setEmail("TEST");
        customer.setAddressDTOS(addresses);
        customer.setId(UUID.randomUUID());
        customer.setName("TEST");
        customer.setPhoneNumber(15313498);
        return Collections.singletonList(customer);
    }
}
