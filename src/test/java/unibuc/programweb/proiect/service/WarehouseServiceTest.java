package unibuc.programweb.proiect.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import unibuc.programweb.proiect.dtos.WarehouseDTO;
import unibuc.programweb.proiect.mapper.WarehouseMapper;
import unibuc.programweb.proiect.model.Warehouse;
import unibuc.programweb.proiect.repository.WarehouseRepository;
import unibuc.programweb.proiect.service.impl.WarehouseServiceImpl;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WarehouseServiceTest {
    @Mock
    WarehouseRepository warehouseRepository;

    @Mock
    WarehouseMapper warehouseMapper;

    @InjectMocks
    WarehouseServiceImpl warehouseService;

    @Test
    void findWarehousesFromCity() {
        String city = "CITY";
        when(warehouseRepository.findAllByCity(city)).thenReturn(warehouseList());
        when(warehouseMapper.toDto(anyList())).thenReturn(warehouseDTOList());

        List<WarehouseDTO> warehousesFromCity = warehouseService.findWarehousesFromCity(city);

        assertEquals(warehousesFromCity.get(0).getCity(), city);
    }

    @Test
    void createWarehouse() {
        WarehouseDTO warehouseDTO = warehouseDTOList().get(0);
        when(warehouseRepository.save(any())).thenReturn(warehouseList().get(0));
        when(warehouseMapper.toDto(any(Warehouse.class))).thenReturn(warehouseDTOList().get(0));
        when(warehouseMapper.toEntity(any(WarehouseDTO.class))).thenReturn(warehouseList().get(0));

        WarehouseDTO savedWarehouse = warehouseService.createWarehouse(warehouseDTOList().get(0));

        assertEquals(savedWarehouse.getCity(), warehouseDTO.getCity());
        assertEquals(savedWarehouse.getAddress(), warehouseDTO.getAddress());

    }

    @Test
    void getWarehouses() {
        when(warehouseRepository.findAll()).thenReturn(warehouseList());
        when(warehouseMapper.toDto(anyList())).thenReturn(warehouseDTOList());

        List<WarehouseDTO> warehouses = warehouseService.getWarehouses();

        assertNotEquals(warehouses.size(), 0);
    }

    private List<Warehouse> warehouseList() {
        Warehouse warehouse = new Warehouse();
        warehouse.setCity("CITY");
        warehouse.setAddress("TEST_ADDRESS");
        return Collections.singletonList(warehouse);
    }
    private List<WarehouseDTO> warehouseDTOList() {
        WarehouseDTO warehouse = new WarehouseDTO();
        warehouse.setCity("CITY");
        warehouse.setAddress("TEST_ADDRESS");
        return Collections.singletonList(warehouse);
    }
}