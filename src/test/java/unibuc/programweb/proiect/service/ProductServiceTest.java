package unibuc.programweb.proiect.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import unibuc.programweb.proiect.dtos.ProductDTO;
import unibuc.programweb.proiect.mapper.ProductMapper;
import unibuc.programweb.proiect.model.Product;
import unibuc.programweb.proiect.repository.ProductRepository;
import unibuc.programweb.proiect.service.impl.ProductServiceImpl;

import java.util.Collections;
import java.util.List;
import java.util.UUID;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @Mock
    ProductRepository productRepository;


    @Mock
    ProductMapper productMapper;

    @InjectMocks
    ProductServiceImpl productService;

    @Test
    void getProducts() {
        List<Product> products1 = returnProducts();
        when(productRepository.findAll()).thenReturn(returnProducts());
        when(productMapper.toDto(any(Product.class))).thenReturn(returnProductsDTO().get(0));

        List<ProductDTO> products = productService.getProducts();


        assertEquals(products.size(), products1.size());
        assertEquals(products.get(0).getProductName(), products1.get(0).getProductName());
        assertEquals(products.get(0).getDescription(), products1.get(0).getDescription());
        assertEquals(products.get(0).getPrice(), products1.get(0).getPrice());

    }

    @Test
    void getProductByOrder() {
        List<Product> products1 = returnProducts();
        when(productRepository.findAllByOrderId(any())).thenReturn(returnProducts());
        when(productMapper.toDto(any(Product.class))).thenReturn(returnProductsDTO().get(0));
        List<ProductDTO> products = productService.getProductByOrder(UUID.randomUUID());

        assertEquals(products.size(), products1.size());
        assertEquals(products.get(0).getProductName(), products1.get(0).getProductName());
        assertEquals(products.get(0).getDescription(), products1.get(0).getDescription());
        assertEquals(products.get(0).getPrice(), products1.get(0).getPrice());
    }

    private List<Product> returnProducts() {
        Product product = new Product();
        product.setDescription("TEST_DESCRIERE");
        product.setPrice(123);
        product.setProductName("PRODUCT_NAME");
        return Collections.singletonList(product);
    }

    private List<ProductDTO> returnProductsDTO() {
        ProductDTO product = new ProductDTO();
        product.setDescription("TEST_DESCRIERE");
        product.setPrice(123);
        product.setProductName("PRODUCT_NAME");
        return Collections.singletonList(product);
    }
}