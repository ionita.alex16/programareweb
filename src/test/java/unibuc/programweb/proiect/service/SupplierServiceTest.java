package unibuc.programweb.proiect.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import unibuc.programweb.proiect.dtos.SupplierDTO;
import unibuc.programweb.proiect.dtos.WarehouseDTO;
import unibuc.programweb.proiect.errors.CustomException;
import unibuc.programweb.proiect.mapper.SupplierMapper;
import unibuc.programweb.proiect.mapper.WarehouseMapper;
import unibuc.programweb.proiect.model.Supplier;
import unibuc.programweb.proiect.model.Warehouse;
import unibuc.programweb.proiect.repository.SupplierRepository;
import unibuc.programweb.proiect.repository.WarehouseRepository;
import unibuc.programweb.proiect.service.impl.SupplierServiceImpl;

import javax.print.DocFlavor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class SupplierServiceTest {
    @Mock
    SupplierRepository supplierRepository;

    @Mock
    SupplierMapper supplierMapper;

    @Mock
    WarehouseRepository warehouseRepository;

    @Mock
    WarehouseMapper warehouseMapper;

    @InjectMocks
    SupplierServiceImpl supplierService;


    @Test
    @DisplayName("add new warehouse to not found supplier")
    void addNewWarehouseToNotFoundSupplierThrowsException() {
        when(supplierRepository.findAllByName(any())).thenReturn(new ArrayList<>());

        CustomException customException = assertThrows(CustomException.class, () -> {
            supplierService.addNewWarehouseToSupplier("TEST", any());
        });

        assertEquals(HttpStatus.NO_CONTENT, customException.getHttpStatus());
        assertEquals("Supplier do not exist", customException.getErrorMessage());
    }

    @Test
    @DisplayName("add new warehouse to supplier with existing warehouse")
    void addNewWarehouseToSupplierWithExistingWarehouse() {
        String name = "TEST";

        when(supplierRepository.findAllByName(name)).thenReturn(returnSuppliers());
        when(warehouseRepository.findFirstByAddress(any())).thenReturn(Optional.of(returnWarehouse()));

        List<SupplierDTO> test = supplierService.addNewWarehouseToSupplier(name, returnWarehouseDTO());

        verify(supplierRepository, times(1)).saveAll(any());

    }

    @Test
    @DisplayName("add new warehouse to supplier without warehouse")
    void addNewWarehouseToSupplierWithoutWarehouse() {
        String name = "TEST";

        when(supplierRepository.findAllByName(name)).thenReturn(returnSuppliers());
        when(warehouseRepository.findFirstByAddress(any())).thenReturn(Optional.empty());

        List<SupplierDTO> test = supplierService.addNewWarehouseToSupplier(name, returnWarehouseDTO());

        verify(supplierRepository, times(1)).saveAll(any());
        verify(warehouseRepository, times(1)).save(any());

    }

    @Test
    void getSuppliers() {
    }

    private List<Supplier> returnSuppliers() {
        Supplier supplier = new Supplier();
        supplier.setAddress("TEST_ADDRESS");
        supplier.setName("TEST_NAME");

        return Collections.singletonList(supplier);
    }

    private Warehouse returnWarehouse() {
        Warehouse warehouse = new Warehouse();
        warehouse.setAddress("TEST_ADDRESS");
        warehouse.setCity("TEST_CITY");

        return warehouse;
    }

    private WarehouseDTO returnWarehouseDTO() {
        WarehouseDTO warehouse = new WarehouseDTO();
        warehouse.setAddress("TEST_ADDRESS");
        warehouse.setCity("TEST_CITY");

        return warehouse;
    }
}