package unibuc.programweb.proiect.service;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import unibuc.programweb.proiect.dtos.AddressDTO;
import unibuc.programweb.proiect.errors.CustomException;
import unibuc.programweb.proiect.mapper.AddressMapper;
import unibuc.programweb.proiect.repository.AddressRepository;
import unibuc.programweb.proiect.repository.CustomerRepository;
import unibuc.programweb.proiect.service.impl.AddressServiceImpl;
import unibuc.programweb.proiect.utils.AddressServiceTestUtils;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AddressServiceTest {

    @Mock
    private AddressRepository addressRepository;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private AddressMapper addressMapper;

    @InjectMocks
    private AddressServiceImpl addressService;


    @BeforeAll
    public static void setup() {

    }


    @Test
    @DisplayName("Test get addresses")
    public void getAddressesTest() {
        when(addressRepository.findAll()).thenReturn(AddressServiceTestUtils.returnAddresses());
        List<AddressDTO> allAddresses = addressService.getAllAddresses();
        assertNotEquals(allAddresses.size(), 0);
    }


    @Test
    @DisplayName("Test address by non existing customer")
    public void findAddressesByNonExistingCustomer() {
        UUID uuid = UUID.randomUUID();
        when(customerRepository.findById(any())).thenReturn(Optional.empty());
        CustomException exception = assertThrows(CustomException.class, () ->
                addressService.findAddressesByCustomer(uuid));
        assertEquals("Customer " + uuid + " not found", exception.getErrorMessage());
    }

    @Test
    @DisplayName("Test address by customer")
    public void findAddressesByCustomer() {
        UUID uuid = UUID.randomUUID();
        when(customerRepository.findById(any())).thenReturn(Optional.of(AddressServiceTestUtils.customer()));
        when(addressRepository.findAllByCustomerId(any())).thenReturn(AddressServiceTestUtils.returnAddresses());
        when(addressMapper.toDto(anyList())).thenReturn(AddressServiceTestUtils.returnAddressesDTO());
        List<AddressDTO> addressesByCustomer = addressService.findAddressesByCustomer(uuid);
        assertNotEquals(addressesByCustomer.size(), 0);
    }
}
