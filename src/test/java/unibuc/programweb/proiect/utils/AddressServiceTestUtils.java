package unibuc.programweb.proiect.utils;

import unibuc.programweb.proiect.dtos.AddressDTO;
import unibuc.programweb.proiect.model.Address;
import unibuc.programweb.proiect.model.Customer;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class AddressServiceTestUtils {

    public static List<Address> returnAddresses() {
        Address address1 = new Address();
        address1.setId(UUID.randomUUID());
        address1.setAddress("TEST ADDRESS1");
        address1.setCity("TEST CITY1");
        address1.setPostalCode("TEST POSTALCODE1");
        address1.setCustomer(customer());

        Address address2 = new Address();
        address2.setId(UUID.randomUUID());
        address2.setAddress("TEST ADDRESS2");
        address2.setCity("TEST CITY2");
        address2.setPostalCode("TEST POSTALCODE2");
        address2.setCustomer(customer());

        Address address3 = new Address();
        address3.setId(UUID.randomUUID());
        address3.setAddress("TEST ADDRESS3");
        address3.setCity("TEST CITY3");
        address3.setPostalCode("TEST POSTALCODE3");
        address3.setCustomer(customer());

        return Arrays.asList(address1, address2, address3);
    }

    public static List<AddressDTO> returnAddressesDTO() {
        AddressDTO address1 = new AddressDTO();
        address1.setId(UUID.randomUUID());
        address1.setAddress("TEST ADDRESS1");
        address1.setCity("TEST CITY1");
        address1.setPostalCode("TEST POSTALCODE1");

        AddressDTO address2 = new AddressDTO();
        address2.setId(UUID.randomUUID());
        address2.setAddress("TEST ADDRESS2");
        address2.setCity("TEST CITY2");
        address2.setPostalCode("TEST POSTALCODE2");

        AddressDTO address3 = new AddressDTO();
        address3.setId(UUID.randomUUID());
        address3.setAddress("TEST ADDRESS3");
        address3.setCity("TEST CITY3");
        address3.setPostalCode("TEST POSTALCODE3");

        return Arrays.asList(address1, address2, address3);
    }

    public static Customer customer() {
        Customer customer = new Customer();
        customer.setEmail("TEST_EMAIL");
        customer.setName("TEST_NAME");
        customer.setPhoneNumber(1235);
        return customer;
    }
}
