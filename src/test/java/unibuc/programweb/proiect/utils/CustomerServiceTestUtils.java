package unibuc.programweb.proiect.utils;

import unibuc.programweb.proiect.model.Customer;

import java.util.Arrays;
import java.util.List;

public class CustomerServiceTestUtils {

    public static List<Customer> returnCustomers() {
        Customer customer1 = new Customer();
        customer1.setEmail("TEST_EMAIL1");
        customer1.setName("TEST_NAME1");
        customer1.setPhoneNumber(1);

        Customer customer2 = new Customer();
        customer2.setEmail("TEST_EMAIL2");
        customer2.setName("TEST_NAME2");
        customer2.setPhoneNumber(2);

        Customer customer3 = new Customer();
        customer3.setEmail("TEST_EMAIL3");
        customer3.setName("TEST_NAME3");
        customer3.setPhoneNumber(3);

        return Arrays.asList(customer1, customer2, customer3);
    }
}
