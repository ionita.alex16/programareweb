package unibuc.programweb.proiect.mapper;

import java.util.List;


/**
 *
 * @param <D> this is the dto
 * @param <E> this is the entity
 */

public interface EntityMapper <D, E> {

    E toEntity (D dto);

    D toDto (E entity);

    List<E> toEntity (List<D> dto);

    List<D> toDto (List<E> entity);




}
