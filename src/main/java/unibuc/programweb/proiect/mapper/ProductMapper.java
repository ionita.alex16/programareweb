package unibuc.programweb.proiect.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import unibuc.programweb.proiect.dtos.ProductDTO;
import unibuc.programweb.proiect.model.Product;

@Mapper(componentModel = "spring", uses = {})
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {

    @Mapping(target = "id", source = "id")
    Product toEntity(ProductDTO productDTO);

    @Mapping(target = "id", source = "id")
    ProductDTO toDto(Product product);
}
