package unibuc.programweb.proiect.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import unibuc.programweb.proiect.dtos.OrderDTO;
import unibuc.programweb.proiect.model.Order;

@Mapper(componentModel = "spring", uses = {})
public interface OrderMapper extends EntityMapper<OrderDTO, Order> {

    @Mapping(target = "id", source = "id")
    Order toEntity(OrderDTO orderDTO);

    @Mapping(target = "id", source = "id")
    OrderDTO toDto(Order order);
}
