package unibuc.programweb.proiect.mapper;

import org.mapstruct.Mapper;
import unibuc.programweb.proiect.dtos.AddressDTO;
import unibuc.programweb.proiect.model.Address;

@Mapper(componentModel = "spring", uses = {})
public interface AddressMapper extends EntityMapper<AddressDTO, Address> {

    Address toEntity (AddressDTO addressDTO);

    AddressDTO toDto (Address address);

}
