package unibuc.programweb.proiect.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import unibuc.programweb.proiect.dtos.CustomerDTO;
import unibuc.programweb.proiect.model.Customer;

@Mapper(componentModel = "spring", uses = {})
public interface CustomerMapper extends EntityMapper<CustomerDTO, Customer> {

    @Mapping(target = "addresses", source = "addressDTOS")
    Customer toEntity(CustomerDTO addressDTO);

    @Mapping(target = "addressDTOS", source = "addresses")
    CustomerDTO toDto(Customer address);
}
