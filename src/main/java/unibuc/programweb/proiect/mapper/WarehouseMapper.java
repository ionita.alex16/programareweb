package unibuc.programweb.proiect.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import unibuc.programweb.proiect.dtos.OrderDTO;
import unibuc.programweb.proiect.dtos.WarehouseDTO;
import unibuc.programweb.proiect.model.Order;
import unibuc.programweb.proiect.model.Warehouse;

@Mapper(componentModel = "spring", uses = {})
public interface WarehouseMapper extends EntityMapper<WarehouseDTO, Warehouse> {

    @Mapping(target = "id", source = "id")
    Warehouse toEntity(WarehouseDTO warehouseDTO);

    @Mapping(target = "id", source = "id")
    WarehouseDTO toDto(Warehouse warehouse);
}
