package unibuc.programweb.proiect.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import unibuc.programweb.proiect.dtos.OrderDTO;
import unibuc.programweb.proiect.dtos.SupplierDTO;
import unibuc.programweb.proiect.model.Order;
import unibuc.programweb.proiect.model.Supplier;

@Mapper(componentModel = "spring", uses = {})
public interface SupplierMapper extends EntityMapper<SupplierDTO, Supplier> {

    @Mapping(target = "id", source = "id")
    Supplier toEntity(SupplierDTO supplierDTO);

    @Mapping(target = "id", source = "id")
    SupplierDTO toDto(Supplier supplier);
}
