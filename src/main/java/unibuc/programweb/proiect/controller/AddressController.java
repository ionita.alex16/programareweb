package unibuc.programweb.proiect.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import unibuc.programweb.proiect.dtos.AddressDTO;
import unibuc.programweb.proiect.service.AddressService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/address")
public class AddressController {

    private final Logger log = LoggerFactory.getLogger(AddressController.class);

    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    /**
     * This endpoint return all addresses from db
     *
     * @return list of all addresses
     */
    @GetMapping("")
    public ResponseEntity<List<AddressDTO>> getAllAddresses() {
        log.debug("Request for all addresses");
        List<AddressDTO> addresses = addressService.getAllAddresses();
        return new ResponseEntity<>(addresses, HttpStatus.OK);
    }

    /**
     * @param city the city after which addresses will be searched
     * @return list of addresses that is in the specific city
     */

    @GetMapping("/byCity")
    public ResponseEntity<List<AddressDTO>> findAddressesFromSpecificRegion(@RequestParam String city) {
        log.debug("Request for getting addresses that is in the city " + city);
        List<AddressDTO> addressesFromSpecificCity = addressService.findAddressesFromSpecificCity(city);
        return new ResponseEntity<>(addressesFromSpecificCity, HttpStatus.OK);
    }

    @GetMapping("/byCustomer")
    public ResponseEntity<List<AddressDTO>> findCustomerAddresses(@RequestParam UUID customerId) {
        log.debug("Request for getting all addresses of customer " + customerId);
        List<AddressDTO> addressesByCustomer = addressService.findAddressesByCustomer(customerId);

        return new ResponseEntity<>(addressesByCustomer, HttpStatus.OK);
    }
}
