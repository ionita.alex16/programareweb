package unibuc.programweb.proiect.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import unibuc.programweb.proiect.dtos.CustomerDTO;
import unibuc.programweb.proiect.dtos.OrderDTO;
import unibuc.programweb.proiect.dtos.OrderForCustomerDTO;
import unibuc.programweb.proiect.service.OrderService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("")
    public ResponseEntity<List<OrderDTO>> getAllOrders() {
        List<OrderDTO> orders = orderService.getOrders();

        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @GetMapping("byCustomer")
    public ResponseEntity<List<OrderDTO>> findOrderByCustomer(@RequestParam UUID customerId) {
        List<OrderDTO> orderByCustomer = orderService.findOrderByCustomer(customerId);

        return new ResponseEntity<>(orderByCustomer, HttpStatus.CREATED);
    }

    @PostMapping("/customer")
    public ResponseEntity<CustomerDTO> createOrderForSpecificCustomer(@RequestBody OrderForCustomerDTO orderForCustomer) {
        CustomerDTO orderForSpecificCustomer = orderService.createOrderForSpecificCustomer(orderForCustomer);

        return new ResponseEntity<>(orderForSpecificCustomer, HttpStatus.CREATED);
    }
}
