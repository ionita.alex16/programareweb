package unibuc.programweb.proiect.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import unibuc.programweb.proiect.dtos.WarehouseDTO;
import unibuc.programweb.proiect.service.WarehouseService;

import java.util.List;

@RestController
@RequestMapping("/api/warehouse")
public class WarehouseController {

    private final WarehouseService warehouseService;

    public WarehouseController(WarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    @PostMapping("")
    public ResponseEntity<WarehouseDTO> createWarehouse(@RequestParam WarehouseDTO warehouseDTO) {
        WarehouseDTO warehouse = warehouseService.createWarehouse(warehouseDTO);

        return new ResponseEntity<>(warehouse, HttpStatus.CREATED);
    }

    @GetMapping("")
    public ResponseEntity<List<WarehouseDTO>> findWarehouses() {
        List<WarehouseDTO> warehouses = warehouseService.getWarehouses();

        return new ResponseEntity<>(warehouses, HttpStatus.CREATED);
    }

    @GetMapping("/city")
    public ResponseEntity<List<WarehouseDTO>> getWarehousesFromCity(@RequestParam String city) {
        List<WarehouseDTO> warehousesFromCity = warehouseService.findWarehousesFromCity(city);

        return new ResponseEntity<>(warehousesFromCity, HttpStatus.OK);
    }
}
