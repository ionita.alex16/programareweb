package unibuc.programweb.proiect.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import unibuc.programweb.proiect.dtos.SupplierDTO;
import unibuc.programweb.proiect.dtos.WarehouseDTO;
import unibuc.programweb.proiect.service.SupplierService;

import java.util.List;

@RestController
@RequestMapping("/api/supplier")
public class SupplierController {

    private final SupplierService supplierService;

    public SupplierController(SupplierService supplierService) {
        this.supplierService = supplierService;
    }

    @GetMapping("")
    public ResponseEntity<List<SupplierDTO>> getSuppliers() {
        List<SupplierDTO> suppliers = supplierService.getSuppliers();

        return ResponseEntity.ok(suppliers);
    }

    @PostMapping("")
    public ResponseEntity<SupplierDTO> saveSupplier(@RequestBody SupplierDTO supplierDTO) {
        SupplierDTO savedSupplier = supplierService.saveSupplier(supplierDTO);

        return new ResponseEntity<>(savedSupplier, HttpStatus.CREATED);
    }

    @PostMapping("/createBulk")
    public ResponseEntity<List<SupplierDTO>> saveBulkSuppliers(@RequestBody List<SupplierDTO> bulkSuppliers) {
        List<SupplierDTO> supplierDTOS = supplierService.saveBulkSuppliers(bulkSuppliers);

        return new ResponseEntity<>(supplierDTOS, HttpStatus.CREATED);
    }

    @PutMapping("/addWarehouse")
    public ResponseEntity<List<SupplierDTO>> addNewWarehouseToSupplier(@RequestParam String supplierName, @RequestBody WarehouseDTO warehouseDTO) {
        List<SupplierDTO> supplierDTOS = supplierService.addNewWarehouseToSupplier(supplierName, warehouseDTO);

        return new ResponseEntity<>(supplierDTOS, HttpStatus.ACCEPTED);
    }
}
