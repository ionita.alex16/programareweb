package unibuc.programweb.proiect.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import unibuc.programweb.proiect.dtos.ProductDTO;
import unibuc.programweb.proiect.service.ProductService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/product")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("")
    public ResponseEntity<List<ProductDTO>> getProducts() {
        List<ProductDTO> products = productService.getProducts();

        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/byOrder")
    public ResponseEntity<List<ProductDTO>> getProductsByOrder(@RequestParam UUID orderId) {
        List<ProductDTO> products = productService.getProductByOrder(orderId);

        return new ResponseEntity<>(products, HttpStatus.OK);
    }
}
