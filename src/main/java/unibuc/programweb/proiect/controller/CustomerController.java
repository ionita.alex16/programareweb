package unibuc.programweb.proiect.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import unibuc.programweb.proiect.dtos.CustomerDTO;
import unibuc.programweb.proiect.service.CustomerService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    private final Logger log = LoggerFactory.getLogger(CustomerController.class);

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    /**
     * This api is returning a list with all customers
     *
     * @return all customers
     */
    @GetMapping("")
    public ResponseEntity<List<CustomerDTO>> getAllCustomers() {
        log.debug("Request for all customers");
        List<CustomerDTO> customers = customerService.findAllCustomers();
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    /**
     * This api is saving a customer, and then returned
     *
     * @param customerDTO
     * @return savedCustomer
     */
    @PostMapping("")
    public ResponseEntity<CustomerDTO> createCustomer(@RequestBody CustomerDTO customerDTO) {
        log.debug("Request for creating customer, " + customerDTO);
        CustomerDTO customer = customerService.createCustomer(customerDTO);
        return new ResponseEntity<>(customer, HttpStatus.CREATED);
    }

    /**
     * @param city the city where the customer is located
     * @return list of customers lives in the specific city
     */

    @GetMapping("/byCity")
    public ResponseEntity<List<CustomerDTO>> findCustomersInSpecificCity(@RequestParam String city) {
        log.debug("Request for getting customers that is in the city " + city);
        List<CustomerDTO> customersByCity = customerService.findCustomersByCity(city);

        return new ResponseEntity<>(customersByCity, HttpStatus.OK);
    }

    /**
     *
     * @param customerDTO
     * @return updated customer
     */
    @PutMapping("")
    public ResponseEntity<CustomerDTO> updateCustomerInformation(@RequestBody CustomerDTO customerDTO) {
        log.debug("Request for updating customers with id " + customerDTO.getId());
        CustomerDTO updatedCustomer = customerService.updateCustomer(customerDTO);
        return new ResponseEntity<>(updatedCustomer, HttpStatus.OK);
    }
}
