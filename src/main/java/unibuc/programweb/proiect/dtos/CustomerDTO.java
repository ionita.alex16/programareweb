package unibuc.programweb.proiect.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import unibuc.programweb.proiect.model.Address;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class CustomerDTO implements Serializable {
    private UUID id;
    private String name;
    private String email;
    private Integer phoneNumber;
    private List<AddressDTO> addressDTOS;
    private List<OrderDTO> orders;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }


    public List<AddressDTO> getAddressDTOS() {
        return addressDTOS;
    }

    public void setAddressDTOS(List<AddressDTO> addressDTOS) {
        this.addressDTOS = addressDTOS;
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", addressDTOS=" + addressDTOS +
                '}';
    }

    public List<OrderDTO> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderDTO> orders) {
        this.orders = orders;
    }
}
