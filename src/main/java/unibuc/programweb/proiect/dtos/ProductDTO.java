package unibuc.programweb.proiect.dtos;

import unibuc.programweb.proiect.enums.ProductType;

import java.io.Serializable;
import java.util.UUID;

public class ProductDTO implements Serializable {

    private UUID id;
    private String productName;
    private Integer price;
    private String description;
    private ProductType productType;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }
}
