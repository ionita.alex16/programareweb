package unibuc.programweb.proiect.dtos;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class SupplierDTO implements Serializable {
    private UUID id;
    private String name;
    private String postalCode;
    private String address;
    private String productsSupplier;
    private List<WarehouseDTO> warehouses;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<WarehouseDTO> getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(List<WarehouseDTO> warehouses) {
        this.warehouses = warehouses;
    }

    public String getProductsSupplier() {
        return productsSupplier;
    }

    public void setProductsSupplier(String productsSupplier) {
        this.productsSupplier = productsSupplier;
    }
}
