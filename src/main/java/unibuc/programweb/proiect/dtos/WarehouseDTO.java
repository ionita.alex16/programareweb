package unibuc.programweb.proiect.dtos;

import java.io.Serializable;
import java.util.UUID;

public class WarehouseDTO implements Serializable {

    private UUID id;
    private String address;
    private String city;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
