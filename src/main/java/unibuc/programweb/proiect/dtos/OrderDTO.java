package unibuc.programweb.proiect.dtos;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class OrderDTO implements Serializable {

    private UUID id;
    private String orderDetails;
    private List<ProductDTO> products;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(String orderDetails) {
        this.orderDetails = orderDetails;
    }

    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }
}
