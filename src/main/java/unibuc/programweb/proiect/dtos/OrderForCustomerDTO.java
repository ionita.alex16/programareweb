package unibuc.programweb.proiect.dtos;

import java.util.UUID;

public class OrderForCustomerDTO {
    private UUID customerId;
    private OrderDTO order;

    public UUID getCustomerId() {
        return customerId;
    }

    public void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }

    public OrderDTO getOrder() {
        return order;
    }

    public void setOrder(OrderDTO order) {
        this.order = order;
    }
}
