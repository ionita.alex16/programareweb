package unibuc.programweb.proiect.service;

import unibuc.programweb.proiect.dtos.SupplierDTO;
import unibuc.programweb.proiect.dtos.WarehouseDTO;

import java.util.List;

public interface SupplierService {

    SupplierDTO saveSupplier(SupplierDTO supplierDTO);

    List<SupplierDTO> saveBulkSuppliers(List<SupplierDTO> bulkSuppliers);

    List<SupplierDTO> addNewWarehouseToSupplier(String supplierName, WarehouseDTO warehouseDTO);

    List<SupplierDTO> getSuppliers();

}
