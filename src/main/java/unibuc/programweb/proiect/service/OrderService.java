package unibuc.programweb.proiect.service;

import unibuc.programweb.proiect.dtos.CustomerDTO;
import unibuc.programweb.proiect.dtos.OrderDTO;
import unibuc.programweb.proiect.dtos.OrderForCustomerDTO;
import unibuc.programweb.proiect.model.Order;

import java.util.List;
import java.util.UUID;

public interface OrderService {

    List<OrderDTO> getOrders();

    List<OrderDTO> findOrderByCustomer(UUID customerId);

    void saveOrdersWithProducts(List<Order> orders);

    CustomerDTO createOrderForSpecificCustomer(OrderForCustomerDTO orderForCustomer);
}
