package unibuc.programweb.proiect.service;

import unibuc.programweb.proiect.dtos.AddressDTO;
import unibuc.programweb.proiect.model.Address;

import java.util.List;
import java.util.UUID;

public interface AddressService {

    List<AddressDTO> getAllAddresses();

    List<AddressDTO> findAddressesFromSpecificCity(String city);

    List<AddressDTO> findAddressesByCustomer(UUID customerId);
}
