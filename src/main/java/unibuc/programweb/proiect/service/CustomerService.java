package unibuc.programweb.proiect.service;

import unibuc.programweb.proiect.dtos.CustomerDTO;

import java.util.List;
import java.util.UUID;

public interface CustomerService {

    List<CustomerDTO> findAllCustomers();

    CustomerDTO createCustomer(CustomerDTO customerDTO);

    List<CustomerDTO> findCustomersByCity(String city);

    CustomerDTO updateCustomer(CustomerDTO newCustomer);
}
