package unibuc.programweb.proiect.service;

import unibuc.programweb.proiect.dtos.WarehouseDTO;

import java.util.List;

public interface WarehouseService {
    List<WarehouseDTO> findWarehousesFromCity(String city);

    WarehouseDTO createWarehouse(WarehouseDTO warehouse);

    List<WarehouseDTO> getWarehouses();
}
