package unibuc.programweb.proiect.service;

import unibuc.programweb.proiect.dtos.ProductDTO;

import java.util.List;
import java.util.UUID;

public interface ProductService {
    List<ProductDTO> getProducts();

    List<ProductDTO> getProductByOrder(UUID orderId);
}
