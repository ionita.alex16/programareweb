package unibuc.programweb.proiect.service.impl;


import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import unibuc.programweb.proiect.dtos.CustomerDTO;
import unibuc.programweb.proiect.dtos.OrderDTO;
import unibuc.programweb.proiect.dtos.OrderForCustomerDTO;
import unibuc.programweb.proiect.errors.CustomException;
import unibuc.programweb.proiect.mapper.CustomerMapper;
import unibuc.programweb.proiect.mapper.OrderMapper;
import unibuc.programweb.proiect.model.Customer;
import unibuc.programweb.proiect.model.Order;
import unibuc.programweb.proiect.model.Product;
import unibuc.programweb.proiect.model.Supplier;
import unibuc.programweb.proiect.repository.CustomerRepository;
import unibuc.programweb.proiect.repository.OrderRepository;
import unibuc.programweb.proiect.repository.ProductRepository;
import unibuc.programweb.proiect.repository.SupplierRepository;
import unibuc.programweb.proiect.service.OrderService;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final OrderMapper orderMapper;

    private final ProductRepository productRepository;

    private final CustomerRepository customerRepository;

    private final CustomerMapper customerMapper;

    private final SupplierRepository supplierRepository;

    public OrderServiceImpl(OrderRepository orderRepository, OrderMapper orderMapper, ProductRepository productRepository, CustomerRepository customerRepository, CustomerMapper customerMapper, SupplierRepository supplierRepository) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
        this.productRepository = productRepository;
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
        this.supplierRepository = supplierRepository;
    }

    @Override
    public List<OrderDTO> getOrders() {
        List<Order> orders = orderRepository.findAll();

        return orders.stream().map(orderMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<OrderDTO> findOrderByCustomer(UUID customerId) {
        if (customerId == null) {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Customer not found");
        }

        List<Order> customerOrders = orderRepository.findAllByCustomerId(customerId);

        return customerOrders.stream().map(orderMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void saveOrdersWithProducts(List<Order> unsavedOrders) {
        List<Order> orders = orderRepository.saveAll(unsavedOrders);
        for (Order savedOrder : orders) {
            if (!savedOrder.getProducts().isEmpty()) {
                List<Product> products = new ArrayList<>(savedOrder.getProducts());
                products = findSupplierForProduct(products);
                productRepository.saveAll(products.stream().peek(product -> product.setOrder(savedOrder)).collect(Collectors.toList()));
            }
        }
    }

    @Override
    public CustomerDTO createOrderForSpecificCustomer(OrderForCustomerDTO orderForCustomer) {
        if (orderForCustomer.getCustomerId() == null) {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Customer could not be empty");
        }
        Optional<Customer> customerOptional = customerRepository.findById(orderForCustomer.getCustomerId());
        if (!customerOptional.isPresent()) {
            throw new CustomException(HttpStatus.NO_CONTENT.value(), HttpStatus.NO_CONTENT, "Customer not found");
        }

        Customer customer = customerOptional.get();

        Order order = orderMapper.toEntity(orderForCustomer.getOrder());
        order.setCustomer(customer);

        saveOrdersWithProducts(Collections.singletonList(order));
        customer.getOrders().add(order);

        return customerMapper.toDto(customerRepository.save(customer));
    }

    private List<Product> findSupplierForProduct(List<Product> products) {
        List<Product> toBeSavedProducts = new ArrayList<>();
        for (Product product : products) {
            Supplier supplier = supplierRepository.findFirstByProductsSupplier(product.getProductType());
            if (supplier == null) {
                throw new CustomException(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, "Supplier for that type not found");
            }
            product.setProductSupplier(supplier);
            toBeSavedProducts.add(product);
        }
        if (toBeSavedProducts.size() != 0) {
            return productRepository.saveAll(toBeSavedProducts);
        }
        return products;
    }
}
