package unibuc.programweb.proiect.service.impl;


import org.springframework.stereotype.Service;
import unibuc.programweb.proiect.dtos.WarehouseDTO;
import unibuc.programweb.proiect.mapper.WarehouseMapper;
import unibuc.programweb.proiect.model.Warehouse;
import unibuc.programweb.proiect.repository.WarehouseRepository;
import unibuc.programweb.proiect.service.WarehouseService;

import java.util.List;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    private final WarehouseRepository warehouseRepository;

    private final WarehouseMapper warehouseMapper;

    public WarehouseServiceImpl(WarehouseRepository warehouseRepository, WarehouseMapper warehouseMapper) {
        this.warehouseRepository = warehouseRepository;
        this.warehouseMapper = warehouseMapper;
    }

    @Override
    public List<WarehouseDTO> findWarehousesFromCity(String city) {
        List<Warehouse> warehousesInSpecificCity = warehouseRepository.findAllByCity(city);
        return warehouseMapper.toDto(warehousesInSpecificCity);
    }

    @Override
    public WarehouseDTO createWarehouse(WarehouseDTO warehouseDTO) {
        return warehouseMapper.toDto(warehouseRepository.save(warehouseMapper.toEntity(warehouseDTO)));
    }

    @Override
    public List<WarehouseDTO> getWarehouses() {
        List<Warehouse> warehouses = warehouseRepository.findAll();

        return warehouseMapper.toDto(warehouses);
    }
}
