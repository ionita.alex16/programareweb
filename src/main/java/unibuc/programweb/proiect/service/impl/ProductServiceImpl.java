package unibuc.programweb.proiect.service.impl;


import org.springframework.stereotype.Service;
import unibuc.programweb.proiect.dtos.ProductDTO;
import unibuc.programweb.proiect.mapper.ProductMapper;
import unibuc.programweb.proiect.model.Product;
import unibuc.programweb.proiect.repository.ProductRepository;
import unibuc.programweb.proiect.service.ProductService;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    private final ProductMapper productMapper;

    public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    @Override
    public List<ProductDTO> getProducts() {
        List<Product> products = productRepository.findAll();
        return products.stream().map(productMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<ProductDTO> getProductByOrder(UUID orderId) {
        List<Product> allByOrderId = productRepository.findAllByOrderId(orderId);
        return allByOrderId.stream().map(productMapper::toDto).collect(Collectors.toList());
    }
}
