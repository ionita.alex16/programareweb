package unibuc.programweb.proiect.service.impl;


import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import unibuc.programweb.proiect.dtos.SupplierDTO;
import unibuc.programweb.proiect.dtos.WarehouseDTO;
import unibuc.programweb.proiect.errors.CustomException;
import unibuc.programweb.proiect.mapper.SupplierMapper;
import unibuc.programweb.proiect.mapper.WarehouseMapper;
import unibuc.programweb.proiect.model.Supplier;
import unibuc.programweb.proiect.model.Warehouse;
import unibuc.programweb.proiect.repository.SupplierRepository;
import unibuc.programweb.proiect.repository.WarehouseRepository;
import unibuc.programweb.proiect.service.SupplierService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SupplierServiceImpl implements SupplierService {

    private final SupplierRepository supplierRepository;
    private final SupplierMapper supplierMapper;
    private final WarehouseRepository warehouseRepository;
    private final WarehouseMapper warehouseMapper;

    public SupplierServiceImpl(SupplierRepository supplierRepository, SupplierMapper supplierMapper, WarehouseRepository warehouseRepository, WarehouseMapper warehouseMapper) {
        this.supplierRepository = supplierRepository;
        this.supplierMapper = supplierMapper;
        this.warehouseRepository = warehouseRepository;
        this.warehouseMapper = warehouseMapper;
    }

    @Override
    public SupplierDTO saveSupplier(SupplierDTO supplierDTO) {
        Supplier savedSupplier = supplierRepository.save(supplierMapper.toEntity(supplierDTO));

        saveWarehouses(savedSupplier);
        return supplierMapper.toDto(savedSupplier);
    }

    private void saveWarehouses(Supplier savedSupplier) {
        List<Warehouse> collect = savedSupplier.getWarehouses().stream().peek(warehouse -> warehouse.setSupplier(savedSupplier)).collect(Collectors.toList());

        warehouseRepository.saveAll(collect);
    }

    @Override
    public List<SupplierDTO> saveBulkSuppliers(List<SupplierDTO> bulkSuppliers) {
        List<Supplier> savedSuppliers = supplierRepository.saveAll(supplierMapper.toEntity(bulkSuppliers));
        savedSuppliers.forEach(supplier -> {
            List<Warehouse> warehouses = new ArrayList<>(supplier.getWarehouses());
            warehouseRepository.saveAll(warehouses.stream().peek(warehouse -> warehouse.setSupplier(supplier)).collect(Collectors.toList()));
        });
        return supplierMapper.toDto(savedSuppliers);
    }

    @Override
    public List<SupplierDTO> addNewWarehouseToSupplier(String supplierName, WarehouseDTO warehouseDTO) {
        List<Supplier> suppliersWithSpecificName = supplierRepository.findAllByName(supplierName);

        if (suppliersWithSpecificName.size() == 0) {
            throw new CustomException(HttpStatus.NO_CONTENT.value(), HttpStatus.NO_CONTENT, "Supplier do not exist");
        }
        Optional<Warehouse> optionalWarehouse = warehouseRepository.findFirstByAddress(warehouseDTO.getAddress());

        if (optionalWarehouse.isPresent()) {
            Warehouse warehouse = optionalWarehouse.get();
            suppliersWithSpecificName.forEach(supplier -> {
                supplier.getWarehouses().add(warehouse);
            });
            supplierRepository.saveAll(suppliersWithSpecificName);
        } else {
            Warehouse savedWarehouse = warehouseRepository.save(warehouseMapper.toEntity(warehouseDTO));
            suppliersWithSpecificName.forEach(supplier -> {
                supplier.getWarehouses().add(savedWarehouse);
            });
            supplierRepository.saveAll(suppliersWithSpecificName);
        }


        return supplierMapper.toDto(suppliersWithSpecificName);
    }

    @Override
    public List<SupplierDTO> getSuppliers() {
        List<Supplier> suppliers = supplierRepository.findAll();
        return supplierMapper.toDto(suppliers);
    }
}
