package unibuc.programweb.proiect.service.impl;


import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import unibuc.programweb.proiect.dtos.CustomerDTO;
import unibuc.programweb.proiect.errors.CustomException;
import unibuc.programweb.proiect.mapper.CustomerMapper;
import unibuc.programweb.proiect.model.Address;
import unibuc.programweb.proiect.model.Customer;
import unibuc.programweb.proiect.model.Order;
import unibuc.programweb.proiect.repository.AddressRepository;
import unibuc.programweb.proiect.repository.CustomerRepository;
import unibuc.programweb.proiect.service.CustomerService;
import unibuc.programweb.proiect.service.OrderService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    private final CustomerMapper customerMapper;

    private final AddressRepository addressRepository;

    private final OrderService orderService;

    public CustomerServiceImpl(CustomerRepository customerRepository, CustomerMapper customerMapper, AddressRepository addressRepository, OrderService orderService) {
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
        this.addressRepository = addressRepository;
        this.orderService = orderService;
    }

    @Override
    public List<CustomerDTO> findAllCustomers() {
        List<Customer> customers = customerRepository.findAll();
        return customers.stream().map(customerMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public CustomerDTO createCustomer(CustomerDTO customerDTO) {
        if (customerDTO.getName() == null) {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "User could not be save without username");
        }
        Customer savedCustomer = customerRepository.save(customerMapper.toEntity(customerDTO));
        saveSecondaryEntities(savedCustomer);

        return customerMapper.toDto(savedCustomer);
    }

    private void saveSecondaryEntities(Customer savedCustomer) {
        List<Address> collect = savedCustomer.getAddresses()
                .stream().peek(address -> address.setCustomer(savedCustomer))
                .collect(Collectors.toList());

        addressRepository.saveAll(collect);

        List<Order> collectedOrders = savedCustomer.getOrders()
                .stream().peek(order -> order.setCustomer(savedCustomer))
                .collect(Collectors.toList());

        orderService.saveOrdersWithProducts(collectedOrders);
    }

    @Override
    public List<CustomerDTO> findCustomersByCity(String city) {
        List<Customer> customersByCity = customerRepository.findAllByAddressesCity(city);
        return customerMapper.toDto(customersByCity);
    }

    @Override
    public CustomerDTO updateCustomer(CustomerDTO newCustomer) {
        if (newCustomer == null) {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "No customer present");
        }
        if (newCustomer.getId() == null) {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "No customer identifier present");
        }
        Optional<Customer> optionalCustomer = customerRepository.findById(newCustomer.getId());
        if (!optionalCustomer.isPresent()) {
            throw new CustomException(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, "Customer " + newCustomer.getId() + " not found!");
        } else {
            Customer updatedCustomer = customerRepository.save(customerMapper.toEntity(newCustomer));
            return customerMapper.toDto(updatedCustomer);
        }
    }
}
