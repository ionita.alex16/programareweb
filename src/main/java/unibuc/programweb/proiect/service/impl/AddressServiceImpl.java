package unibuc.programweb.proiect.service.impl;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import unibuc.programweb.proiect.dtos.AddressDTO;
import unibuc.programweb.proiect.errors.CustomException;
import unibuc.programweb.proiect.mapper.AddressMapper;
import unibuc.programweb.proiect.model.Address;
import unibuc.programweb.proiect.model.Customer;
import unibuc.programweb.proiect.repository.AddressRepository;
import unibuc.programweb.proiect.repository.CustomerRepository;
import unibuc.programweb.proiect.service.AddressService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;

    private final AddressMapper addressMapper;

    private final CustomerRepository customerRepository;


    public AddressServiceImpl(AddressRepository addressRepository, AddressMapper addressMapper, CustomerRepository customerRepository) {
        this.addressRepository = addressRepository;
        this.addressMapper = addressMapper;
        this.customerRepository = customerRepository;
    }

    @Override
    public List<AddressDTO> getAllAddresses() {
        List<Address> addresses = addressRepository.findAll();
        return addresses.stream().map(addressMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<AddressDTO> findAddressesFromSpecificCity(String city) {
        List<Address> addressesByCity = addressRepository.findAllByCity(city);
        return addressMapper.toDto(addressesByCity);
    }

    @Override
    public List<AddressDTO> findAddressesByCustomer(UUID customerId) {
        Optional<Customer> customer = customerRepository.findById(customerId);
        if (!customer.isPresent()) {
            throw new CustomException(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, "Customer " + customerId + " not found");
        } else {
            List<Address> addressesByCustomer = addressRepository.findAllByCustomerId(customerId);
            return addressMapper.toDto(addressesByCustomer);
        }
    }
}

