package unibuc.programweb.proiect.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "SUPPLIER")
public class Supplier {

    @Id
    @GenericGenerator(name = "generator", strategy = "org.hibernate.id.UUIDGenerator")
    @GeneratedValue(generator = "generator")
    @Column(name = "ID", unique = true, nullable = false)
    private UUID id;

    @Column(name = "NAME", length = 50)
    private String name;

    @Column(name = "POSTAL_CODE", length = 56)
    private String postalCode;

    @Column(name = "ADDRESS", length = 256)
    private String address;

    @OneToMany(mappedBy = "supplier")
    private Set<Warehouse> warehouses = new HashSet<>();

    @OneToMany(mappedBy = "productSupplier")
    private Set<Product> products = new HashSet<>();

    @Column(name = "PRODUCTS_SUPPLIER", length = 50)
    private String productsSupplier;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<Warehouse> getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(Set<Warehouse> warehouses) {
        this.warehouses = warehouses;
    }

    public String getProductsSupplier() {
        return productsSupplier;
    }

    public void setProductsSupplier(String productsSupplier) {
        this.productsSupplier = productsSupplier;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
}
