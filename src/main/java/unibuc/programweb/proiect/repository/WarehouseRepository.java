package unibuc.programweb.proiect.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import unibuc.programweb.proiect.model.Warehouse;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface WarehouseRepository extends JpaRepository<Warehouse, UUID> {

    List<Warehouse> findAllByCity(String city);

    Optional<Warehouse> findFirstByAddress(String address);
}
