package unibuc.programweb.proiect.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import unibuc.programweb.proiect.model.Address;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface AddressRepository extends JpaRepository<Address, UUID> {

    Optional<Address> findById(UUID addressId);

    List<Address> findAllByCity(String city);

    List<Address> findAllByCustomerId(UUID customerId);
}
