package unibuc.programweb.proiect.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import unibuc.programweb.proiect.model.Customer;
import unibuc.programweb.proiect.model.Order;

import java.util.List;
import java.util.UUID;

@Repository
public interface OrderRepository extends JpaRepository<Order, UUID> {

    List<Order> findAllByCustomerId(UUID customerId);
}
