package unibuc.programweb.proiect.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import unibuc.programweb.proiect.model.Product;
import unibuc.programweb.proiect.model.Supplier;

import java.util.List;
import java.util.UUID;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, UUID> {

    Supplier findFirstByProductsSupplier(String product);

    List<Supplier> findAllByName(String name);
}
