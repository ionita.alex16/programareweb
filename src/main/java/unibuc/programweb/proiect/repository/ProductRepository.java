package unibuc.programweb.proiect.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import unibuc.programweb.proiect.model.Customer;
import unibuc.programweb.proiect.model.Product;

import java.util.List;
import java.util.UUID;

@Repository
public interface ProductRepository extends JpaRepository<Product, UUID> {

    List<Product> findAllByOrderId(UUID orderId);
}
