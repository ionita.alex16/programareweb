package unibuc.programweb.proiect.enums;

public enum ProductType {

    TELEFON("TELEFON"), MASINA_SPALAT("MASINA_SPALAT"), FRIGIDER("FRIGIDER");

    private final String type;

    ProductType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
