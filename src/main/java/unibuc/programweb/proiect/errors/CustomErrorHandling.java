package unibuc.programweb.proiect.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CustomErrorHandling {

    private final Logger log = LoggerFactory.getLogger(CustomErrorHandling.class);

    @ExceptionHandler(CustomException.class)
    private ResponseEntity<Object> handleException(CustomException ex) {
        log.error("Custom error: " + ex.getErrorMessage());
        CustomExceptionObject customExceptionObject = new CustomExceptionObject(ex.getStatus(), ex.getErrorMessage(), ex.getHttpStatus());
        return new ResponseEntity<>(customExceptionObject, ex.getHttpStatus() != null ? ex.getHttpStatus() : HttpStatus.INTERNAL_SERVER_ERROR);

    }
}
